const express = require('express');
const nanoid = require('nanoid');
const db = require('../fileDB');

const router = express.Router();

router.get('/', (req, res) => {
  const messages = db.getItems().sort((a, b) => {
    if (a.datetime === b.datetime) {
      return 0;
    } else return a.datetime > b.datetime;
  });

  const startDate = req.query.datetime;

  if (startDate) {
    const date = new Date(startDate);
    if (isNaN(date.getDate())) {
      res.status(400).send({error: "Date is incorrect"});
    } else {
      const messagesAfterDate = messages.filter(message => startDate < message.datetime);
      res.send(messagesAfterDate);
    }
  } else {
    const messagesToShow = 30;
    res.send(messages.slice(0 - messagesToShow));
  }
});

router.post('/', (req, res) => {
  if (!req.body.author || !req.body.message) {
    res.status(400).send({error: "Author and message must be present in the request"});
  } else {
    const message = {
      id: nanoid(),
      datetime: new Date().toISOString(),
      ...req.body,
    };

    db.addItem(message);
    res.send({message: 'OK'});
  }
});

module.exports = router;