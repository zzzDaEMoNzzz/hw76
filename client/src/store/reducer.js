import {
  ADD_MESSAGE_FAILURE,
  ADD_MESSAGE_SUCCESS,
  CHANGE_INPUT_VALUE,
  ERROR_SHOWED,
  GET_MESSAGES_SUCCESS
} from "./actionsTypes";

const initialState = {
  messages: [],
  addMessageAuthor: '',
  addMessageText: '',
  lastMessageDateTime: null,
  error: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CHANGE_INPUT_VALUE:
      return {...state, [action.stateName]: action.value};
    case GET_MESSAGES_SUCCESS:
      if (!action.messages || action.messages.length === 0) return state;

      return {
        ...state,
        messages: [...state.messages, ...action.messages],
        lastMessageDateTime: action.messages[action.messages.length - 1].datetime || null
      };
    case ADD_MESSAGE_FAILURE:
      return {...state, error: action.error};
    case ADD_MESSAGE_SUCCESS:
      return {...state, addMessageText: ''};
    case ERROR_SHOWED:
      return {...state, error: null};
    default:
      return state;
  }
};

export default reducer;