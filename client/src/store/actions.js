import axios from 'axios';

import {
  ADD_MESSAGE_FAILURE,
  ADD_MESSAGE_SUCCESS,
  CHANGE_INPUT_VALUE,
  ERROR_SHOWED,
  GET_MESSAGES_SUCCESS
} from "./actionsTypes";

export const errorShowed = () => ({type: ERROR_SHOWED});

export const changeInputValue = (stateName, value) => ({type: CHANGE_INPUT_VALUE, stateName, value});

export const addMessageSuccess = () => ({type: ADD_MESSAGE_SUCCESS});
export const addMessageFailure = error => ({type: ADD_MESSAGE_FAILURE, error});

export const addMessage = (author, message) => {
  return dispatch => {
    axios.post('http://localhost:8000/messages', {author, message}).then(
      () => {
        dispatch(addMessageSuccess());
      },
      error => {
        const status = error.response.status;
        const message = error.response.data.error;

        dispatch(addMessageFailure({status, message}));
      }
    );
  };
};

export const getMessages = dateTime => {
  return dispatch => {
    let url = 'http://localhost:8000/messages';
    if (dateTime) {
      url += '?datetime=' + dateTime;
    }

    axios.get(url).then(response => {
      console.log(response.data);
      dispatch({type: GET_MESSAGES_SUCCESS, messages: response.data});
    });
  };
};