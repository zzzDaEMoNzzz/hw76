import React, {Component} from "react";
import {connect} from "react-redux";
import "./AddMessageMenu.css";
import {addMessage, changeInputValue} from "../../store/actions";

class AddMessageMenu extends Component {
  onInputChange = (event, stateName) => {
    event.preventDefault();

    this.props.changeInputValue(stateName, event.target.value);
  };

  onFormSubmit = event => {
    event.preventDefault();

    this.props.addMessage(this.props.addMessageAuthor, this.props.addMessageText);
  };

  render() {
    return (
      <form className="AddMessageMenu" onSubmit={this.onFormSubmit}>
        <span>Author</span>
        <input
          type="text"
          value={this.props.addMessageAuthor}
          onChange={event => this.onInputChange(event, 'addMessageAuthor')}
          required
        />
        <span>Message:</span>
        <input
          type="text"
          value={this.props.addMessageText}
          onChange={event => this.onInputChange(event, 'addMessageText')}
          required
        />
        <button className="AddMessageMenu-postBtn">Add</button>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  addMessageAuthor: state.addMessageAuthor,
  addMessageText: state.addMessageText
});

const mapDispatchToProps = dispatch => ({
  changeInputValue: (stateName, value) => dispatch(changeInputValue(stateName, value)),
  addMessage: (author, message) => dispatch(addMessage(author, message))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddMessageMenu);