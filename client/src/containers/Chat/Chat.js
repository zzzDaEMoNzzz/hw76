import React, {Component} from "react";
import {connect} from "react-redux";
import {NotificationContainer, NotificationManager} from "react-notifications";
import '../../../node_modules/react-notifications/lib/notifications.css';
import "./Chat.css";
import AddMessageMenu from "../AddMessageMenu/AddMessageMenu";
import Messages from "../../components/Messages/Messages";
import {errorShowed, getMessages} from "../../store/actions";

class Chat extends Component {
  componentDidMount() {
    this.getMessagesInterval = setInterval(() => {
      this.props.getMessages(this.props.lastMessageDateTime);

      if (this.props.error) {
        NotificationManager.error(this.props.error.message, `Error ${this.props.error.status}!`, 10000);
        this.props.errorShowed();
      }
    }, 2000);
  }

  componentWillUnmount() {
    clearInterval(this.getMessagesInterval);
  }

  render() {
    return (
      <div className="Chat">
        <AddMessageMenu/>
        <Messages messagesArray={this.props.messages}/>
        <NotificationContainer/>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  messages: state.messages,
  lastMessageDateTime: state.lastMessageDateTime,
  error: state.error
});

const mapDispatchToProps = dispatch => ({
  getMessages: dateTime => dispatch(getMessages(dateTime)),
  errorShowed: () => dispatch(errorShowed())
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
